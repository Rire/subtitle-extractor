# Extract and remove embedded subtitles from MKV files

**forked from https://github.com/ScribbleGhost/Extract-and-remove-embedded-subtitles-from-MKV-files, and modified to parse arguments**

# Usage:

**You'll need mkvtoolnix to be installed before procedding:**

$ python3 subextract.py /path/to/folder

**To recursively run, use find:**

$ find /path/to/media/dir/here -type d -exec python3 subextract.py "{}" \\;

**If you want to parallelize it to speed it up a bit, pipe it through GNU parallel:**

$ find /path/to/media/dir/here -type d | parallel --bar -j4 python3 subextract.py "{}" 

**If you wish to have the subs removed from the .mkv file after processing, remove the comment that starts at line 130**
